using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;

namespace DFA
{
    public static class DEFINE {
        public static int MATCH_IS_FAIL = -1;
    }

    public class State {
        private string name;
        private bool finite;

        public State(string name) {
            this.name = name;
            this.finite = false;
        }

        public State(string name, bool initFinite) {
            this.name = name;
            this.finite = initFinite;
        }

        public bool isFinite() {
            return this.finite;
        }
    }

    public class Transition {
        // Set - Delta is Set of Transitions
        // Delta item is a tuple
        // tuple consists of <T1, T2>
        // where T1 is regex, T2 is next state index
        // In details
        // regex as label
        // int as next state index
        private Tuple<Regex, int> r;

        public Transition(string regex, int index) {
            this.r = new Tuple<Regex, int>(new Regex(regex, RegexOptions.Compiled), index);
        }

        public Transition(ref Regex regex, int index) {
            this.r = new Tuple<Regex, int>(regex, index);
        }

        public int next(string str) {
            if(this.r.Item1.IsMatch(str)) {
                return this.r.Item2;
            }
            return DEFINE.MATCH_IS_FAIL;
        }

        public void regexOut(out Regex regex) {
            regex = this.r.Item1;
        }
    }

    public class DFA {
        private List<Tuple<State, List<Transition>>> q;
        private LinkedList<LinkedList<string>> groups;
        private LinkedList<string> errors;
        private LinkedList<string> semanticErrors;
        private int prev;
        private int next;

        public DFA(List<Tuple<State, List<Transition>>> q) {
            this.prev = 0;
            this.next = 0;
            this.errors = new LinkedList<string>();
            this.groups = new LinkedList<LinkedList<string>>();
            this.semanticErrors = new LinkedList<string>();
            this.q = q;

        }

        // RETURN true if f(State_sub{lastStateIndex}) = isFinite = true
        // else return false
        public bool isLangAccepted() {
            return this.q[prev].Item1.isFinite();
        }

        public void recognizeStr(string str) {
            int i, n;
            n = 0;
            i = 0;
            while (i < this.q[next].Item2.Count) {
                if((n = this.q[next].Item2[i].next(str)) != DEFINE.MATCH_IS_FAIL) {
                    break;
                }
                ++i;
            }

            if(i == this.q[next].Item2.Count) {
                this.errors.AddLast(str);
                return;
            }

            Regex r;
            this.q[next].Item2[i].regexOut(out r);
            Match m = r.Match(str);
            LinkedList<string> strlist = new LinkedList<string>();

            do {
                
                for(int a = 1; a < m.Groups.Count; ++a) {
                    strlist.AddLast(m.Groups[a].Value);
                }

            } while ((m = m.NextMatch()).Success);

            this.groups.AddLast(strlist);
            this.prev = this.next;
            this.next = n;

        }

        public void recognizeStr(string str, ref LinkedList<string> results) {
            if(results == null) {
                this.semanticErrors.AddLast("results is null");
                return;
            }
        }

        public void setBeginST() {
            this.next = 0;
        }

        public void setBeginST(int nextST) {
            this.next = nextST;
        }

        public List<string[]> getGroups() {

            List<string[]> strlist = new List<string[]>();
            
            foreach(var group in groups) {
                strlist.Add(group.ToArray<string>());
            }

            return strlist;
        }
    }

    public class test {

        private DFA dfa;
        private Action<string> terminal = (string str) => { Console.WriteLine(str); };
        private List<Tuple<State, List<Transition>>> dfa_settings;
        private string[] testdata;

        public bool loadDfaSettings(string path) {
          if(!File.Exists(path)) {
            terminal("File not exist");
            return false;
          }

          XmlDocument xdoc = new XmlDocument();
          xdoc.Load(path);

          XmlNode root = xdoc.DocumentElement;

          LinkedList<Tuple<State, LinkedList<Transition>>> list
           = new LinkedList<Tuple<State, LinkedList<Transition>>>();

          foreach(XmlNode nodeState in root.ChildNodes) {
            if(nodeState.Name != "state") {
              terminal("node name not state. name is [" + nodeState.Name + "]");
              return false;
            }

            string name = nodeState.Name;
            bool finite;
            if(!bool.TryParse(nodeState.Attributes["finite"].Value, out finite)) {
              terminal("parse attr fininte [" + nodeState.Attributes["finite"].Value + "] is disaster");
              return false;
            }

            State state = new State(name, finite);

            LinkedList<Transition> trans = new LinkedList<Transition>();
            foreach(XmlNode nodeTrans in nodeState) {

              string regex = nodeTrans.Attributes["regex"].Value;
              int moveToState;

              if(!int.TryParse(nodeTrans.Attributes["moveToState"].Value, out moveToState)) {
                terminal("parse attr moveToState [" + nodeTrans.Attributes["moveToState"].Value + "] is disaster");
                return false;
              }

              trans.AddLast(new Transition(regex, moveToState));
            }

            list.AddLast(new Tuple<State, LinkedList<Transition>>(state, trans));
          }

          if(list.Count == 0) {
            terminal("xml settings file not found dfa states");
            return false;
          } 

          this.dfa_settings = new List<Tuple<State, List<Transition>>>();
          foreach(var item in list) {
            this.dfa_settings.Add(new Tuple<State, List<Transition>>(item.Item1, item.Item2.ToList()));
          }

          return true;
        }

        public void init_default_settings() {
            dfa = new DFA(
                new List<Tuple<State, List<Transition>>>() {

                    // INIT STATE AND TRANSITIONS
                    new Tuple<State, List<Transition>>(
                        new State("S0"), 
                        new List<Transition>() {
                            new Transition(@"(\w+)", 1)
                    }),

                    new Tuple<State, List<Transition>>(
                        new State("S1", true), 
                        new List<Transition>() {
                            new Transition(@"(\d+)", 1)
                    })
                }
            );
        }

        public bool init_with_settings() {
          if(this.dfa_settings == null) {
            terminal("dfa settings is null");
            return false;
          }

          this.dfa = new DFA(this.dfa_settings);
          return true;
        }

        public bool loadTestData(string path) {
          if(!File.Exists(path)) {
            terminal("[ ERROR ] file not exist");
            return false;
          }

          string[] arr = File.ReadAllLines(path);

          if(arr.Length == 0) {
            terminal("arr length is nil");
            return false;
          }
          
          this.testdata = arr;

          return true;
        }

        public void printGroups() {
            foreach(var g in dfa.getGroups()) {
                terminal("group {");
                foreach(var elem in g) {
                    Console.WriteLine(" " + elem);
                }
                terminal("}\n");
            }
        }

        public void run_test_default() {
            this.dfa.setBeginST();
            this.dfa.recognizeStr("word ford");
            this.dfa.recognizeStr("1984");

            Console.WriteLine("accepted: " + this.dfa.isLangAccepted());

            printGroups(); 
        }

        public void run_test_with_testdata() {
          this.dfa.setBeginST();
          foreach(var line in testdata) {
            this.dfa.recognizeStr(line);
          }
          terminal("accepted " + this.dfa.isLangAccepted().ToString());
          printGroups();
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
          //const string arg_dfa_setttings = "--dfa";
          //const string arg_data_sequence = "--data";

          //string dfa_path = "";
          //string data_path = "";

          if(args.Length != 2) {
            Console.WriteLine("[ ERROR ] args quant");
            return;
          }

          test t = new test();
          
          
          if(t.loadDfaSettings(args[0])) {
            Console.WriteLine("[ OK ] load settings");
          } else {
            Console.WriteLine("[ ERROR ] load settings");
            return;
          }

          if(t.loadTestData(args[1])) {
            Console.WriteLine("[ OK ] load test data");
          } else {
            Console.WriteLine("[ ERROR ] load test data");
            return;
          }

          t.init_with_settings();

          t.run_test_with_testdata();


        }
    }
}
